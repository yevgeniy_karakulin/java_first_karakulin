package com.javadev.marathon.karakuliny;

public class TaskCh04N115 {
    public static void main(String[] args) {
        System.out.println(getDescription(1989));
    }

    public static String getDescription(int year) {
        int animal = (year - 1984) % 12;
        int color = (year - 1984) % 10;
        String animalInWord;
        String colorInWord;
        switch (animal) {
            case 0:
                animalInWord = "Крыса, ";
                break;
            case 1:
                animalInWord = "Корова, ";
                break;
            case 2:
                animalInWord = "Тигр, ";
                break;
            case 3:
                animalInWord = "Заяц, ";
                break;
            case 4:
                animalInWord = "Дракон, ";
                break;
            case 5:
                animalInWord = "Змея, ";
                break;
            case 6:
                animalInWord = "Лошадь, ";
                break;
            case 7:
                animalInWord = "Овца, ";
                break;
            case 8:
                animalInWord = "Обезьяна, ";
                break;
            case 9:
                animalInWord = "Петух, ";
                break;
            case 10:
                animalInWord = "Собака, ";
                break;
            case 11:
                animalInWord = "Свинья, ";
                break;
            default:
                animalInWord = " ";
        }
        switch (color) {
            case 0:
            case 1:
                colorInWord = "Зеленый";
                break;
            case 2:
            case 3:
                colorInWord = "Красный";
                break;
            case 4:
            case 5:
                colorInWord = "Желтый";
                break;
            case 6:
            case 7:
                colorInWord = "Белый";
                break;
            case 8:
            case 9:
                colorInWord = "Черный";
                break;
            default:
                colorInWord = " ";
        }
        return animalInWord + colorInWord;
    }
}