package com.javadev.marathon.karakuliny;

public class TaskCh09N022 {
    public static void main(String[] args) {
        System.out.println(getHalf("SLOVAR"));
    }

    public static String getHalf(String word) {
        StringBuffer result = new StringBuffer();
        result.append(word, 0, word.length() / 2);
        return result.toString();
    }
}