package com.javadev.marathon.karakuliny;

public class TaskCh04N033 {
    public static boolean isEven(int n) {
        return n % 2 == 0;
    }

    public static boolean isOdd(int n) {
        return n % 2 != 0;
    }
}