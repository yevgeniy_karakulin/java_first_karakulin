package com.javadev.marathon.karakuliny;

public class TaskCh05N038 {
    public static void main(String[] args) {
        int iterations = 10;
        System.out.println(getDistance(iterations));
        System.out.println(getMileage(iterations));
    }

    public static double getDistance(int iterations) {
        double distance = 0;
        int direction = 1;
        for (int i = 1; i <= iterations; i++) {
            distance += (double) 1 / i * direction;
            direction *= -1;
        }
        return distance;
    }

    public static double getMileage(int iterations) {
        double mileage = 0;
        for (int i = 1; i <= iterations; i++) {
            mileage += (double) 1 / i;
        }
        return mileage;
    }
}