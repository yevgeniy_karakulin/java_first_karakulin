package com.javadev.marathon.karakuliny;

public class TaskCh09N166 {
    public static void main(String[] args) {
        System.out.println(changeWords("where are you from"));
    }

    public static String changeWords(String statement) {
        String words[] = statement.split(" ");
        String temp = words[0];
        words[0] = words[words.length - 1];
        words[words.length - 1] = temp;
        StringBuilder result = new StringBuilder();
        for (String s : words) {
            result.append(s);
            result.append(" ");
        }
        return result.toString();
    }
}