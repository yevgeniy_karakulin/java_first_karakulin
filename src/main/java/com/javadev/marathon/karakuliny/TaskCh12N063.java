package com.javadev.marathon.karakuliny;

public class TaskCh12N063 {
    public static void main(String[] args) {
        for (int i = 0; i < 11; i++) {
            for (int k = 0; k < 4; k++) {
                System.out.print(fillArray(11, 4)[i][k] + "   ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < getAverage(fillArray(11, 4)).length; i++) {
            System.out.println(getAverage(fillArray(11, 4))[i]);
        }
    }

    public static int[][] fillArray(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < m; k++) {
                array[i][k] = (int) (Math.random() * 10 + 10);
            }
        }
        return array;
    }

    public static double[] getAverage(int[][] array) {
        double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            double sum = 0;
            for (int k = 0; k < array[i].length; k++) {
                sum += array[i][k];
            }
            result[i] = (double) sum / array[i].length;
        }
        return result;
    }
}