package com.javadev.marathon.karakuliny;

public class TaskCh10N050 {
    public static void main(String[] args) {
        System.out.println(getAckerman(1, 3));
    }

    public static int getAckerman(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return getAckerman(n - 1, 1);
        }
        return getAckerman(n - 1, getAckerman(n, m - 1));
    }
}