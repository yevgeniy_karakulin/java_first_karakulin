package com.javadev.marathon.karakuliny;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your number: ");
        int enteredNumber = scanner.nextInt();
        System.out.println("You entered number :" + enteredNumber);
    }
}