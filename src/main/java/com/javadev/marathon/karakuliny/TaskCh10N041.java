package com.javadev.marathon.karakuliny;

public class TaskCh10N041 {
    public static void main(String[] args) {
        System.out.println(getFactorial(5));
    }

    public static int getFactorial(int n) {
        if (n > 0) {
            return n * getFactorial(n - 1);
        } else {
            return 1;
        }
    }
}