package com.javadev.marathon.karakuliny;

public class TaskCh02N013 {
    public static int revertNumber(int number) {
        return number % 10 * 100 + number / 10 % 10 * 10 + number / 100;
    }
}