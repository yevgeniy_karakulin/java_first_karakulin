package com.javadev.marathon.karakuliny;

public class TaskCh12N234 {
    public static void main(String[] args) {
        for (int i = 0; i < 11; i++) {
            for (int k = 0; k < 11; k++) {
                System.out.print(removeColumn(fillArray(11, 11), 4)[i][k] + "   ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < 11; i++) {
            for (int k = 0; k < 11; k++) {
                System.out.print(removeLine(fillArray(11, 11), 4)[i][k] + "   ");
            }
            System.out.println();
        }
    }

    public static int[][] fillArray(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < m; k++) {
                array[i][k] = (int) (Math.random() * 10 + 10);
            }
        }
        return array;
    }

    public static int[][] removeLine(int[][] array, int rline) {
        for (int line = 0; line < array.length; line++) {
            if (line >= rline && line < array.length - 2) {
                for (int col = 0; col < array[line].length; col++) {
                    array[line][col] = array[line + 1][col];
                }
            } else if (line == array.length - 1) {
                for (int col = 0; col < array[line].length; col++) {
                    array[line][col] = 0;
                }
            }
        }
        return array;
    }

    public static int[][] removeColumn(int[][] array, int rcolumn) {
        for (int col = 0; col < array.length; col++) {
            if (col >= rcolumn && col < array[col].length - 2) {
                for (int line = 0; line < array.length; line++) {
                    array[line][col] = array[line][col + 1];
                }
            } else if (col == array.length - 1) {
                for (int line = 0; line < array.length; line++) {
                    array[line][col] = 0;
                }
            }
        }
        return array;
    }
}