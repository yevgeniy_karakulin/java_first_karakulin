package com.javadev.marathon.karakuliny;

public class TaskCh04N036 {

    public static String getColor(int minutes) {
        if (minutes % 5 < 3) {
            return "green";
        } else {
            return "red";
        }
    }
}