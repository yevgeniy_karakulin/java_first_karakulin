package com.javadev.marathon.karakuliny;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskCh10N053 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> collectedIntegers = new ArrayList<>();
        while (true) {
            int number = scanner.nextInt();
            if (number == 0) {
                break;
            }
            collectedIntegers.add(number);
            scanner.nextLine();
        }
        System.out.println(recReverse(collectedIntegers, collectedIntegers.size()).toString());
    }

    public static ArrayList recReverse(ArrayList<Integer> arrayList, int size) {
        List<Integer> reversedList = new ArrayList<>();
        if (size > arrayList.size() / 2) {
            int temp = arrayList.get(size - 1);
            arrayList.set(size - 1, arrayList.get(arrayList.size() - size));
            arrayList.set(arrayList.size() - size, temp);
            return recReverse(arrayList, size - 1);
        } else {
            return arrayList;
        }
    }
}