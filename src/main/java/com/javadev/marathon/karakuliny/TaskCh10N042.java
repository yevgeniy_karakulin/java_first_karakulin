package com.javadev.marathon.karakuliny;

public class TaskCh10N042 {
    public static void main(String[] args) {
        System.out.println(inPower(10, 10));
    }

    public static long inPower(int a, int n) {
        if (n > 1) {
            return a * inPower(a, n - 1);
        } else if (n == 0) {
            return 1;
        } else {
            return a;
        }
    }
}