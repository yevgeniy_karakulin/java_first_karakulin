package com.javadev.marathon.karakuliny;

public class TaskCh10N048 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 5, 10, 7, 6, 5, 4, 3, 2, 1};
        System.out.println(getMaxValue(array, array.length));
    }

    public static int getMaxValue(int[] array, int size) {
        if (size > 1) {
            return Math.max(array[size - 1], getMaxValue(array, size - 1));
        } else {
            return array[size];
        }
    }
}