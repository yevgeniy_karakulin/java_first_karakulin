package com.javadev.marathon.karakuliny;

public class TaskCh10N052 {
    public static void main(String[] args) {
        int[] tests = new int[]{0, 1, 5, 10, 11, 15, 100, 123, 555, 375, 1000, 1040, 12345};
        for (int test : tests) {
            System.out.printf("%6d | %d%n", test, recReverse(test, 0));
        }
    }

    public static int recReverse(int n, int result) {
        if (n > 9) {
            return recReverse(n / 10, result * 10 + n % 10);
        } else {
            return result * 10 + n;
        }
    }
}