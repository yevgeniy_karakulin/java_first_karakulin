package com.javadev.marathon.karakuliny;

public class TaskCh09N107 {
    public static void main(String[] args) {
        System.out.println(changeLetters("aaoo"));
    }

    public static boolean aoPresent(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == 'a') {
                for (int j = 0; j < word.length(); j++) {
                    if (word.charAt(j) == 'o') {
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }

    public static String changeLetters(String word) {
        if (aoPresent(word)) {
            StringBuilder result = new StringBuilder(word);
            result.replace(result.indexOf("a"), result.indexOf("a") + 1, "o");
            result.replace(result.lastIndexOf("o"), result.lastIndexOf("o") + 1, "a");
            return result.toString();
        } else {
            return "There is no \'a\' or \'o\' in this word";
        }
    }
}