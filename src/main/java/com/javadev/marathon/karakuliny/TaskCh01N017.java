package com.javadev.marathon.karakuliny;

public class TaskCh01N017 {

    public static double getO(double x) {
        return Math.sqrt(1 - Math.pow(Math.sin(x), 2));
    }

    public static double getP(double x, double a, double b, double c) {
        return Math.pow((a * Math.pow(x, 2) + b * x + c), -0.5);
    }

    public static double getR(double x) {
        return (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / 2 * Math.sqrt(x);
    }

    public static double getS(double x) {
        return Math.abs(x) + Math.abs(x + 1);
    }
}