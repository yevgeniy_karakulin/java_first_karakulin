package com.javadev.marathon.karakuliny;

public class TaskCh03N029 {
    public static void main(String[] args) {
        System.out.println(methodE(5,13,22));
    }

    public static boolean methodA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    public static boolean methodB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    public static boolean methodC(int x, int y) {
        return x == 0 || y == 20;
    }

    public static boolean methodD(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    public static boolean methodE(int x, int y, int z) {
        return (x % 5 == 0 ^ y % 5 == 0) ^ (z % 5 == 0 ^ y % 5 == 0);
    }

    public static boolean methodF(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}