package com.javadev.marathon.karakuliny;

public class TaskCh12N024 {
    public static void main(String[] args) {
        int n = 7;

        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                System.out.print(fillArrayPatternB(n)[i][k] + "   ");
            }
            System.out.println();
        }
    }

    public static int[][] fillArrayPatternA(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                if (i == 0 || k == 0) {
                    array[i][k] = 1;
                } else {
                    array[i][k] = array[i][k - 1] + array[i - 1][k];
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternB(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                int insert = i + k + 1;
                if (insert > n) {
                    insert -= n;
                }
                array[i][k] = insert;
            }
        }
        return array;
    }
}