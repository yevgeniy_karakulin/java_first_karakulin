package com.javadev.marathon.karakuliny;

public class TaskCh09N017 {
    public static void main(String[] args) {
        System.out.println(compareFirstLast("tibet"));
    }

    public static boolean compareFirstLast(String word) {
        return word.charAt(0) == word.charAt(word.length() - 1);
    }
}