package com.javadev.marathon.karakuliny;

public class TaskCh10N047 {
    public static void main(String[] args) {
        System.out.println(getKthFibonacciNumber(7));
    }

    public static long getKthFibonacciNumber(int k) {
        if (k == 1 | k == 2) {
            return 1;
        } else {
            return getKthFibonacciNumber(k - 1) + getKthFibonacciNumber(k - 2);
        }
    }
}