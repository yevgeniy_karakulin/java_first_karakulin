package com.javadev.marathon.karakuliny;

public class TaskCh09N015 {
    public static void main(String[] args) {
        System.out.println(getChar("SLOVO", 3));
    }

    public static char getChar(String word, int k) {
        return word.charAt(k);
    }
}