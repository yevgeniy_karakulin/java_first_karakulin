package com.javadev.marathon.karakuliny;

public class TaskCh04N015 {

    public static int getAge(int toDayYear, int toDayMonth, int birthYear, int birthMonth) {
        int birthDayPassed = 0;
        if (birthMonth <= toDayMonth) {
            birthDayPassed = 1;
        }
        return toDayYear - birthYear + birthDayPassed - 1;
    }
}