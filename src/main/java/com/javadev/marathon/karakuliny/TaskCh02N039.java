package com.javadev.marathon.karakuliny;

public class TaskCh02N039 {
    public static final double HOURANGLE = 6;
    public static final double SECONDSANGLE = 1.0 / 6;

    public static double calculateAngle(int h, int m, int s) {
        if (h >= 12) {
            h -= 12;
        }
        return h * HOURANGLE + m + s * SECONDSANGLE;
    }
}