package com.javadev.marathon.karakuliny;

public class TaskCh04N067 {

    public static String defineDay(int dayOfWeek) {
        if (dayOfWeek % 7 < 6) {
            return "Workday";
        } else {
            return "Weekend";
        }
    }
}