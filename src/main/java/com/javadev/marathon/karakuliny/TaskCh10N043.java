package com.javadev.marathon.karakuliny;

public class TaskCh10N043 {
    public static void main(String[] args) {
        System.out.println(countDigitsSum(1234));
        System.out.println(countDigitsNumber(1234));
    }

    public static int countDigitsSum(int n) {
        if (n >= 10) {
            return n % 10 + countDigitsSum(n / 10);
        } else {
            return n;
        }
    }

    public static int countDigitsNumber(int n) {
        if (n >= 10) {
            return 1 + countDigitsNumber(n / 10);
        } else {
            return 1;
        }
    }
}