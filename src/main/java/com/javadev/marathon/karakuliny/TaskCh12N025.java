package com.javadev.marathon.karakuliny;

public class TaskCh12N025 {
    public static void main(String[] args) {
        int n = 12;
        int m = 10;

        for (int i = 0; i < n; i++) {
            for (int k = 0; k < m; k++) {
                System.out.print(fillArrayPatternR(n, m)[i][k] + "   ");
            }
            System.out.println();
        }
    }

    public static int[][] fillArrayPatternA(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < m; k++) {
                array[i][k] = count++;
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternB(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < m; i++) {
            for (int k = 0; k < n; k++) {
                array[k][i] = count++;
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternV(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < n; i++) {
            for (int k = m - 1; k >= 0; k--) {
                array[i][k] = count++;
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternG(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = n - 1; i >= 0; i--) {
            for (int k = 0; k < m; k++) {
                array[i][k] = count++;
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternD(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                for (int k = 0; k < m; k++) {
                    array[i][k] = count++;
                }
            } else {
                for (int k = m - 1; k >= 0; k--) {
                    array[i][k] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternE(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < m; i++) {
            if (i % 2 == 0) {
                for (int k = 0; k < n; k++) {
                    array[k][i] = count++;
                }
            } else {
                for (int k = n - 1; k >= 0; k--) {
                    array[k][i] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternZh(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < m; i++) {
            if (i % 2 == 0) {
                for (int k = 0; k < n; k++) {
                    array[k][i] = count++;
                }
            } else {
                for (int k = n - 1; k >= 0; k--) {
                    array[k][i] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternZ(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = m - 1; i >= 0; i--) {
            for (int k = 0; k < n; k++) {
                array[k][i] = count++;
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternI(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = n - 1; i >= 0; i--) {
            for (int k = m - 1; k >= 0; k--) {
                array[i][k] = count++;
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternK(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = m - 1; i >= 0; i--) {
            for (int k = n - 1; k >= 0; k--) {
                array[k][i] = count++;
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternL(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = n - 1; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int k = m - 1; k >= 0; k--) {
                    array[i][k] = count++;
                }
            } else {
                for (int k = 0; k < m; k++) {
                    array[i][k] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternM(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                for (int k = m - 1; k >= 0; k--) {
                    array[i][k] = count++;
                }
            } else {
                for (int k = 0; k < m; k++) {
                    array[i][k] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternN(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = m - 1; i >= 0; i--) {
            if (i % 2 == 1) {
                for (int k = 0; k < n; k++) {
                    array[k][i] = count++;
                }
            } else {
                for (int k = n - 1; k >= 0; k--) {
                    array[k][i] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternO(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = 0; i < m; i++) {
            if (i % 2 == 1) {
                for (int k = 0; k < n; k++) {
                    array[k][i] = count++;
                }
            } else {
                for (int k = n - 1; k >= 0; k--) {
                    array[k][i] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternP(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = n - 1; i >= 0; i--) {
            if (i % 2 == 1) {
                for (int k = m - 1; k >= 0; k--) {
                    array[i][k] = count++;
                }
            } else {
                for (int k = 0; k < m; k++) {
                    array[i][k] = count++;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternR(int n, int m) {
        int[][] array = new int[n][m];
        int count = 1;
        for (int i = m - 1; i >= 0; i--) {
            if (i % 2 == 1) {
                for (int k = n - 1; k >= 0; k--) {
                    array[k][i] = count++;
                }
            } else {
                for (int k = 0; k < n; k++) {
                    array[k][i] = count++;
                }
            }
        }
        return array;
    }
}