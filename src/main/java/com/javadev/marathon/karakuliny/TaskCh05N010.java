package com.javadev.marathon.karakuliny;

import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Exchange Rate: ");
        double rate = scanner.nextDouble();
        for (int i = 1; i <= 20; i++) {
            System.out.println(i + " USD      " + i * rate + " RUB");
        }
    }
}