package com.javadev.marathon.karakuliny;

public class TaskCh10N046 {
    public static void main(String[] args) {
        System.out.println(getNthMember(1, 2, 10));
        System.out.println(getSumOfNthMembers(1, 2, 10));
    }

    public static int getNthMember(int start, int gain, int n) {
        if (n > 1) {
            return getNthMember(start * gain, gain, n - 1);
        } else {
            return start;
        }
    }

    public static int getSumOfNthMembers(int start, int gain, int n) {
        if (n > 1) {
            return start + getSumOfNthMembers(start * gain, gain, n - 1);
        } else {
            return start;
        }
    }
}