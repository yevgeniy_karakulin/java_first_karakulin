package com.javadev.marathon.karakuliny;

public class TaskCh10N045 {
    public static void main(String[] args) {
        System.out.println(getNthMember(1, 2, 4));
        System.out.println(getSumOfNthMembers(1, 2, 4));
    }

    public static int getNthMember(int start, int diff, int n) {
        if (n > 1) {
            return getNthMember(start + diff, diff, n - 1);
        } else {
            return start;
        }
    }

    public static int getSumOfNthMembers(int start, int diff, int n) {
        if (n > 1) {
            return start + getSumOfNthMembers(start + diff, diff, n - 1);
        } else {
            return start;
        }
    }
}