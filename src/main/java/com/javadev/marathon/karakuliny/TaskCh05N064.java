package com.javadev.marathon.karakuliny;

public class TaskCh05N064 {
    public static final int POPULATION_FACTOR = 1000;

    public static double getDensity(int x[], double y[]) {
        double density = 0;
        for (int i = 0; i < x.length; i++) {
            density += x[i] * POPULATION_FACTOR / y[i];
        }
        return density;
    }
}