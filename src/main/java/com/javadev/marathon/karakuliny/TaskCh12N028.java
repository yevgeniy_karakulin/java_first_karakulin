package com.javadev.marathon.karakuliny;

public class TaskCh12N028 {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            for (int k = 0; k < 5; k++) {
                System.out.print(fillArray(5)[i][k] + "   ");
            }
            System.out.println();
        }
    }

    public static int[][] fillArray(int n) {
        int[][] array = new int[n][n];
        int count = 1;
        for (int i = 0; i < n; i++) {
            for (int k = i; k < n - 1 - i; k++) {
                array[i][k] = count++;
            }
            for (int k = i; k < n - i; k++) {
                array[k][n - 1 - i] = count++;
            }
            for (int k = n - 2 - i; k >= i + 1; k--) {
                array[n - 1 - i][k] = count++;
            }
            for (int k = n - 1 - i; k >= i + 1; k--) {
                array[k][i] = count++;
            }
        }
        return array;
    }
}
