package com.javadev.marathon.karakuliny;

public class TaskCh10N049 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 5, 10, 7, 6, 5, 4, 3, 2, 1};
        System.out.println(maxIndex(array, 0));
    }

    public static int maxIndex(int[] array, int start) {
        if (isMaxElement(array, array[start]))
            return start;
        return maxIndex(array, start + 1);
    }

    public static boolean isMaxElement(int[] array, int element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] > element)
                return false;
        }
        return true;
    }
}