package com.javadev.marathon.karakuliny;

public class TaskCh12N023 {
    public static void main(String[] args) {
        int n = 9;
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                System.out.print(fillArrayPatternC(n)[i][k] + " ");
            }
            System.out.println();
        }
    }

    public static int[][] fillArrayPatternA(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                if (i == k) {
                    array[i][k] = 1;
                } else if (i + k == 6) {
                    array[i][k] = 1;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternB(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                if (i == k || i + k == 6) {
                    array[i][k] = 1;
                } else if (i == n / 2 || k == n / 2) {
                    array[i][k] = 1;
                }
            }
        }
        return array;
    }

    public static int[][] fillArrayPatternC(int n) {
        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                if (k >= i && n - 1 - i >= k) {
                    array[i][k] = 1;
                } else if (k <= i && n - 1 - i <= k) {
                    array[i][k] = 1;
                }
            }
        }
        return array;
    }
}