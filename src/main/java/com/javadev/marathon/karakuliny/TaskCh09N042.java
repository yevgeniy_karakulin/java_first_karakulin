package com.javadev.marathon.karakuliny;

public class TaskCh09N042 {
    public static void main(String[] args) {
        System.out.println(reverse("SLOVO"));
    }

    public static String reverse(String word) {
        StringBuilder result = new StringBuilder(word);
        return result.reverse().toString();
    }
}
