package com.javadev.marathon.karakuliny;

public class TaskCh02N043 {

    public static double isDivisible(int a, int b) {
        int m1 = a % b;
        int m2 = b % a;
        return m1 * m2 + 1;
    }
}