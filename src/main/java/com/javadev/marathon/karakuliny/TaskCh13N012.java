package com.javadev.marathon.karakuliny;

import java.util.ArrayList;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Database database = new Database();
        Employee ivanov = new Employee("Иванов", "Петр", "Петрович", "Москва, Петушки", 12, 2015);
        database.saveEmployee(ivanov);
        Employee petrov = new Employee("Петров", "Иван", "Петрович", "Москва, Лужники", 11, 2014);
        database.saveEmployee(petrov);
        Employee sidorov = new Employee("Сидоров", "Петр", "Иванович", "Москва, Должники", 9, 2017);
        database.saveEmployee(sidorov);
        Employee noname = new Employee("Васечкин", "Вася", "Москва, Вокзал", 5, 2012);
        database.saveEmployee(noname);
        System.out.println("----------------------------");
        for (Employee employee : database.findByWorkedYears(3)) {
            System.out.print(employee.getName() + " " + employee.getMiddleName() + " " + employee.getSurname() + " /" + employee.getAddress());
            System.out.println();
        }
        System.out.println("----------------------------");
        for (Employee employee : database.findByString("вася")) {
            System.out.print(employee.getName() + " " + employee.getMiddleName() + " " + employee.getSurname() + " " + employee.getAddress());
            System.out.println();
        }
    }
}

class Employee {
    private String surname;
    private String name;
    private String middleName;
    private String address;
    private int monthStartEmployment;
    private int yearStartEmployment;

    public Employee(String surname, String name, String middleName, String address, int monthStartEmployment, int yearStartEmployment) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.address = address;
        this.monthStartEmployment = monthStartEmployment;
        this.yearStartEmployment = yearStartEmployment;
    }

    public Employee(String surname, String name, String address, int monthStartEmployment, int yearStartEmployment) {
        this(surname, name, " ", address, monthStartEmployment, yearStartEmployment);
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getAddress() {
        return address;
    }

    public int getMonthStartEmployment() {
        return monthStartEmployment;
    }

    public int getYearStartEmployment() {
        return yearStartEmployment;
    }
}

class Database {

    ArrayList<Employee> employees = new ArrayList<>();

    public void saveEmployee(Employee employee) {
        employees.add(employee);
    }

    public ArrayList<Employee> findByString(String searchPhrase) {
        ArrayList<Employee> searchResult = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getName().toLowerCase().contains(searchPhrase) || employee.getMiddleName().toLowerCase().contains(searchPhrase) || employee.getSurname().toLowerCase().contains(searchPhrase)) {
                searchResult.add(employee);
            }
        }
        return searchResult;
    }

    public ArrayList<Employee> findByWorkedYears(int years) {
        int currentYear = 2018;
        int currentMonth = 12;
        int yearsWorked;
        ArrayList<Employee> searchResult = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getMonthStartEmployment() >= currentMonth) {
                yearsWorked = currentYear - employee.getYearStartEmployment() + 1;
            } else {
                yearsWorked = currentYear - employee.getYearStartEmployment();
            }
            if (yearsWorked >= years) {
                searchResult.add(employee);
            }
        }
        return searchResult;
    }
}