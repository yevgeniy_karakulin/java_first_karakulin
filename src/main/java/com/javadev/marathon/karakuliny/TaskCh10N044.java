package com.javadev.marathon.karakuliny;

public class TaskCh10N044 {
    public static void main(String[] args) {
        System.out.println(getDigitalSquare(999999988));
    }

    public static int countDigitsSum(int n) {
        if (n >= 10) {
            return n % 10 + countDigitsSum(n / 10);
        } else {
            return n;
        }
    }

    public static int getDigitalSquare(int n) {
        if (n >= 10) {
            return getDigitalSquare(countDigitsSum(n));
        } else {
            return n;
        }
    }
}