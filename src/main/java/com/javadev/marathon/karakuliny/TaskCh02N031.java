package com.javadev.marathon.karakuliny;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter the n number(100 ≤ n ≤ 999): ");
            int n = scanner.nextInt();
            if (n >= 100 && n <= 999) {
                System.out.println("x = " + recombineNumber(n));
                break;
            } else {
                System.out.println("The n not in range (100 ≤ n ≤ 999), try again");
            }
        }
    }

    private static int recombineNumber(int n) {
        return n / 100 * 100 + n % 10 * 10 + n / 10 % 10;
    }
}