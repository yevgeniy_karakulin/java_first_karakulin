package com.javadev.marathon.karakuliny;

public class TaskCh09N185 {

    public static void main(String[] args) {
        System.out.println(checkParentheses("((x+y)+c)*(y-x)"));
    }

    public static String checkParentheses(String mathExpression) {
        char chars[] = mathExpression.toCharArray();
        int left = 0;
        int right = 0;
        int position = 0;
        for (char c : chars) {
            if (c == '(') {
                left += 1;
            }
            if (c == ')') {
                right += 1;
            }
            if (right > left) {
                return "Нет, лишняя правая скобка на позиции: " + position;
            }
            position++;
        }
        if (left > right) {
            return "Нет, лишние левые скобки - " + (left - right) + " шт";
        }
        return "Да, все правильно!";
    }
}