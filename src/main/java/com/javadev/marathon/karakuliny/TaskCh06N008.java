package com.javadev.marathon.karakuliny;

public class TaskCh06N008 {
    public static void main(String[] args) {
        System.out.println(getDoubles(100));
    }

    public static String getDoubles(int n) {
        String result = "";
        for (int i = 1; i * i < n; i++) {
            result += i * i + ", ";
        }
        return result;
    }
}